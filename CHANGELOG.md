# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [Released]

### [1.0] - 2022-09-23

Initial version.

### [1.0] - 2023-08-22

Text unchanged. Published as website.

### [Draft]

[Forum Wiki Post](https://discuss.coding.social/t/social-coding-community-participation-guidelines-wiki-post/137/2): Adaptation of Mozilla Community Participation Guidelines 3.1